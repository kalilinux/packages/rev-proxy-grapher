#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

echo "TEST: Generate a graph"
rev-proxy-grapher --topology /usr/share/doc/rev-proxy-grapher/examples/topology.yaml
if [ ! -f graph.png ]; then
    echo "A problem has occured: graph.png does not exist"
    exit 1
fi
rm graph.png

echo "TEST: Generate a graph with --nmap-xml"
rev-proxy-grapher --topology /usr/share/doc/rev-proxy-grapher/examples/topology.yaml --nmap-xml /usr/share/doc/rev-proxy-grapher/examples/nmap-external.xml --out graph-with-nmap.svg
if [ ! -f graph-with-nmap.svg ]; then
    echo "A problem has occured: graph-with-nmap.svg does not exist"
    exit 1
fi
rm graph-with-nmap.svg
